#!/bin/sh

docker stop prod
docker container rm prod
docker image rm img_prod
docker volume rm vol_prod
docker volume create --name vol_prod --opt device=$PWD --opt o=bind --opt type=none
docker build -t img_prod -f ./project/docker/DockerfileAPI .
docker run -d -p 5555:5555 --mount source=vol_prod,target=/mnt/app/ --name prod -e INFRA_TP5_DB_TYPE=MYSQL -e INFRA_TP5_DB_HOST=db-tp5-infra.ddnsgeek.com -e INFRA_TP5_DB_PORT=7777 -e INFRA_TP5_DB_USER=produser -e INFRA_TP5_DB_PASSWORD=3ac4d0b0e24871436f45275890a458c6 img_prod
